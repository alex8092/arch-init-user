# Created by newuser for 5.3.1

autoload -Uz promptinit
promptinit
prompt clint

setopt histignorealldups sharehistory

HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

autoload -Uz compinit
compinit

bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line
